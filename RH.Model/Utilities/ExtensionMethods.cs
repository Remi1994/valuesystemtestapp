﻿
using RH.Model.Entities;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Cryptography;
using System.Text;


namespace RH.Model.Utilities
{
    public static class ExtensionMethods
    {
        /// <summary>
        /// Used to convert IEnumerable into ObservableCollection
        /// </summary>
        /// <typeparam name="T">The Type of the list</typeparam>
        /// <param name="thisCollection"></param>
        /// <returns></returns>
        public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable<T> thisCollection)
        {
            if (thisCollection == null) return null;
            var oc = new ObservableCollection<T>(thisCollection);
            return oc;
        }
       
        #region Build Tree Tools
        public static List<Group> BuildComboTree(this IEnumerable<Group> source)
        {
            List<Group> result = new List<Group>();
            var groups = source.GroupBy(i => i.ParentId).ToList();
            if (groups.Count > 0)
            {
                var roots = groups.FirstOrDefault(g => g.Key.HasValue == false).ToList();
                if (roots.Count > 0)
                {
                    var dict = groups.Where(g => g.Key.HasValue).ToDictionary(g => g.Key.Value, g => g.ToList());
                    for (int i = 0; i < roots.Count; i++)
                    {
                        result.Add(roots[i]);
                        roots[i].TreeName = "• " + roots[i].Name;
                        AddChildrenCombo(roots[i], dict, ref result, null);
                    }
                }
            }
            return result;
        }
        private static void AddChildrenCombo(Group node, IDictionary<int, List<Group>> source, ref List<Group> result, Group parent)
        {
            node.Level = parent != null ? parent.Level + 1 : 1;
            if (source.ContainsKey(node.Id))
            {
                node.AddedChildren = source[node.Id];
                for (int i = 0; i < node.AddedChildren.Count; i++)
                {
                    node.AddedChildren[i].TreeName = new string(' ', node.Level * 4) + "• " + node.AddedChildren[i].Name;
                    result.Add(node.AddedChildren[i]);
                    AddChildrenCombo(node.AddedChildren[i], source, ref result, node);
                }
            }
            else
            {
                node.AddedChildren = new List<Group>();
            }
        }

        #endregion
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace RH.Model.Entities
{
    public class Group : BaseEntity
    {
        public Group()
        {
            Children = new HashSet<Group>();
          //  AddedChildren = new List<Group>();
            Products = new HashSet<Product>();
        }
        [Required]
        [Index(IsUnique =true)]
        [MaxLength(50)]
        public string Name { get; set; }
        [Required]
        [Index(IsUnique = true)]
        [MaxLength(50)]
        public string TreeName { get; set; }
        public  ICollection<Product> Products { get; set; }
        public int? ParentId { get; set; }
        [ForeignKey("ParentId")]
        public  Group Parent { get; set; }
        [InverseProperty("Parent")]
        public  ICollection<Group> Children { get; set; }
        
        [NotMapped]
        public List<Group> AddedChildren { get; set; }

        [NotMapped]
        public int Level { get; set; }
    }
}

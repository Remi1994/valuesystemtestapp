﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;



namespace RH.Model.Entities
{
    public class Product : BaseEntity
    {
        public Product()
        {
            
        }
        [Required]
        [Index(IsUnique = true)]
        [MaxLength(50)]
        public string Name { get; set; }
        public string Barcode { get; set; }
        public string Description { get; set; }
        public int? GroupId { get; set; }
        public Group Group { get; set; }
        public double Price { get; set; }

    }
}

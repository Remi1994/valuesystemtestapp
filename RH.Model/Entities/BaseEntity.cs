﻿namespace RH.Model.Entities
{
    public class BaseEntity
    {
        public int Id { get; set; }
        public bool IsValid { get; set; }
    }
}

﻿using RH.DbInitializer;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RH.Providers.Group
{
    public class DbGroupProvider : BaseGroupProvider
    {
        protected Application_Context DbContext { get; }
        protected readonly bool disposeContext;
        #region ctor
        public DbGroupProvider()
        {
            DbContext = new Application_Context();
            disposeContext = true;
        }

        public DbGroupProvider(Application_Context context)
        {
            DbContext = context;
            disposeContext = false;
        }
        #endregion
        #region methods 
        public override void AddOrUpdate(Model.Entities.Group model)
        {
            if (model.Id == 0)
            {
                DbContext.Groups.Add(model);
            }
            else
            {
                var modelInDb = DbContext.Groups.SingleOrDefault(c => c.Id == model.Id);
                if (modelInDb == null) throw new ObjectNotFoundException();
                modelInDb.Level = model.Level;
                modelInDb.IsValid = model.IsValid;
                modelInDb.TreeName = model.TreeName;
                modelInDb.Name = model.Name;
                modelInDb.Level = model.Level;
                modelInDb.Parent = model.Parent;
                modelInDb.ParentId = model.ParentId;
            }
            DbContext.SaveChanges();
        }
        public override void Delete(int id)
        {
            var model = DbContext.Groups.SingleOrDefault(m => m.Id == id);
            if (model == null) throw new ObjectNotFoundException();
            DbContext.Groups.Remove(model);
            DbContext.SaveChanges();
        }

        public override IQueryable<Model.Entities.Group> Query(bool includeRelations)
        {
            var query = DbContext.Groups.AsQueryable();
            if (includeRelations) query = query.Include(model => model.Children)/*.Include(model => model.AddedChildren)*/.Include(model => model.Products).Include(model => model.Parent);
            return query;
        }
        #endregion
    }
}

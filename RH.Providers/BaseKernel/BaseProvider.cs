﻿using System;


namespace RH.Providers.BaseKernel
{
    public abstract class BaseProvider : IDisposable
    {
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
        }
    }
}

﻿using System.Linq;


namespace RH.Providers.BaseKernel
{
    public abstract class BaseModelProvider<T> : BaseProvider
    {
        public abstract IQueryable<T> Query(bool includeRelations);
        public abstract void AddOrUpdate(T model);
        public abstract void Delete(int id);
    }
}

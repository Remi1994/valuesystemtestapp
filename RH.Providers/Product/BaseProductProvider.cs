﻿using RH.Providers.BaseKernel;


namespace RH.Providers.Product
{
    public abstract class BaseProductProvider : BaseModelProvider<Model.Entities.Product>
    {
    }
}

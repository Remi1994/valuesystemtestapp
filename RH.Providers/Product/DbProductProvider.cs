﻿using RH.DbInitializer;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Linq;


namespace RH.Providers.Product
{
    public class DbProductProvider : BaseProductProvider
    {
        protected Application_Context DbContext { get; }
        protected readonly bool disposeContext;
        #region ctor
        public DbProductProvider()
        {
            DbContext = new Application_Context();
            disposeContext = true;
        }

        public DbProductProvider(Application_Context context)
        {
            DbContext = context;
            disposeContext = false;
        }
        #endregion
        #region methods 
        public override void AddOrUpdate(RH.Model.Entities.Product model)
        {
            if (model.Id == 0)
            {
                DbContext.Products.Add(model);
            }
            else
            {
                var modelInDb = DbContext.Products.SingleOrDefault(c => c.Id == model.Id);

                if (modelInDb == null) throw new ObjectNotFoundException();
                modelInDb.Barcode = model.Barcode;
                modelInDb.IsValid = model.IsValid;
                modelInDb.Description = model.Description;
                modelInDb.Name = model.Name;
                modelInDb.Price = model.Price;
                modelInDb.GroupId = model.GroupId;
            }
            DbContext.SaveChanges();
        }
        public override void Delete(int id)
        {
            var model = this.Query(true).SingleOrDefault(m => m.Id == id);
            if (model == null) throw new ObjectNotFoundException();
            DbContext.Products.Remove(model);
            DbContext.SaveChanges();
        }

        public override IQueryable<Model.Entities.Product> Query(bool includeRelations)
        {
            var query = DbContext.Products.AsQueryable();
            if (includeRelations) query = query .Include(model => model.Group);
            return query;
        }

      
        #endregion

    }
}

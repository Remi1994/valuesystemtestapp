﻿namespace RH.DbInitializer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial_DB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Groups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        TreeName = c.String(nullable: false, maxLength: 50),
                        ParentId = c.Int(),
                        IsValid = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Groups", t => t.ParentId)
                .Index(t => t.Name, unique: true)
                .Index(t => t.TreeName, unique: true)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        Barcode = c.String(),
                        Description = c.String(),
                        GroupId = c.Int(),
                        Price = c.Double(nullable: false),
                        IsValid = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Groups", t => t.GroupId)
                .Index(t => t.Name, unique: true)
                .Index(t => t.GroupId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "GroupId", "dbo.Groups");
            DropForeignKey("dbo.Groups", "ParentId", "dbo.Groups");
            DropIndex("dbo.Products", new[] { "GroupId" });
            DropIndex("dbo.Products", new[] { "Name" });
            DropIndex("dbo.Groups", new[] { "ParentId" });
            DropIndex("dbo.Groups", new[] { "TreeName" });
            DropIndex("dbo.Groups", new[] { "Name" });
            DropTable("dbo.Products");
            DropTable("dbo.Groups");
        }
    }
}

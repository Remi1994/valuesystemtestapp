﻿using System;
using System.IO;


namespace RH.DbInitializer
{
    public static class Util
    {
        private const string ConnectionStringFileName = "Connection.string";
         
        public const string DefaultConnectionString = @"Data source=.\SQLEXPRESS;Database = AppRHDb;Integrated Security = True;";
        public static string ConnectionString { get { return DefaultConnectionString; } }

        private static string getConnectionString()
        {
            var filePath = Path.Combine(GetAppPath(), ConnectionStringFileName);

            if (!System.IO.File.Exists(filePath))
                System.IO.File.WriteAllText(filePath, DefaultConnectionString);

            return System.IO.File.ReadAllText(filePath);
        }
        public static string GetAppPath()
        {
            var path = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                "RH",
                 "Settings" ,
                 "ConnectionString");

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            return path;

            //return System.IO.Path.GetDirectoryName(System.Reflection.Assembly
            //          .GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");
        }
    }
}

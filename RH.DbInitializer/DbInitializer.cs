﻿
using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Diagnostics;
 

namespace RH.DbInitializer
{
    public class DbInitializer : IDatabaseInitializer<Application_Context>
    {
        public void InitializeDatabase(Application_Context context)
        {
            if (context.Database.Exists())
            {
                #region we use this code when we dont have script for database and we have changes in scenario
                //if (!context.Database.CompatibleWithModel(true))
                //{
                //var config = new Configuration
                //{
                //    AutomaticMigrationDataLossAllowed = false,
                //    AutomaticMigrationsEnabled = false,
                //};
                //var migrator = new DbMigrator(config);
                //try
                //{
                //    migrator.Update();
                //}
                //catch (Exception ex)
                //{
                //    Debug.WriteLine(ex.Message);
                //    throw;
                //}
                //}
                #endregion
            }
            else
            {
                context.Database.Create();
                var transaction = context.Database.BeginTransaction();
                try
                {
                    Seed(context);
                    transaction.Commit();
                }
                catch (Exception excep)
                {
                    throw excep;
                }
            }

            context.SaveChanges();
        }

      
        private void Seed(Application_Context context)
        {
            
        }
    }
}

﻿using RH.Model.Entities;
using System.Data.Entity;


namespace RH.DbInitializer
{
    public class Application_Context : DbContext
    {
        public Application_Context() : base(Util.ConnectionString)
        {
            Database.SetInitializer(new DbInitializer());
        }

         public DbSet<Product> Products { get; set; }
         public DbSet<Group> Groups { get; set; }
         

    }
}

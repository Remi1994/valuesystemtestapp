﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using WPF.Core.Views;

namespace WPF.Core.Navigation
{
    public class View
    {
        public View(String panelKey)
        {
            ViewPanelKey = panelKey;
        }
        #region fields
        private static Dictionary<string, BaseViewPanel> viewPanels = new Dictionary<string, BaseViewPanel>();
        protected static Dictionary<string, BaseViewPanel> ViewPanels
        {
            get { return viewPanels; }
            set { viewPanels = value; }
        }

 
        private string viewPanelKey;
        public string ViewPanelKey
        {
            get { return viewPanelKey; }
            set { viewPanelKey = value; }
        }

        private ObservableCollection<BaseUC> historyLst;
        /// <summary>
        /// Used to store the navigation History   
        /// </summary>
        public ObservableCollection<BaseUC> HistoryLst
        {
            get { if (historyLst == null) historyLst = new ObservableCollection<BaseUC>(); return historyLst; }
            set { historyLst = value; }
        }
        #endregion
         
        #region Navigate Methods
        public virtual void Navigate(BaseUC view)
        {
            //Close the last UC (if exist)
            if (HistoryLst.Count > 1)
            {
                HistoryLst.Last().RemoveData();
            }
            //Add the new Usercontrol to the history list
            HistoryLst.Add(view);
            //Get target panel
            var View = ViewPanels[ViewPanelKey];
            //Clear the view
            View.ViewPanel.Children.Clear();
            //Start loading and Refresh the View
            View.StartLoading();
            view.Refresh();
            View.StopLoading();
            //Display the View on  ViewPanel
            View.ViewPanel.Children.Add(view);
        }

        /// <summary>
        /// Close Current View and open the pervious one 
        /// </summary>
        /// <param name="refresh">
        /// indicates that if the pervious view need to be refreshed or not
        /// </param>
        public virtual void CloseCurrentView(bool refresh = true)
        {
            HistoryLst.Last().RemoveData();
            //Remove the last Usercontrol
            HistoryLst.RemoveAt(HistoryLst.Count - 1);
            //Get the prevoius Usercontrol
            var view = HistoryLst.Last();

            if (refresh)
            {
                view.Refresh();
            }
            //Get target panel
            var View = ViewPanels[ViewPanelKey];
            //Clean the view and display the prevoius Usercontrol
            View.ViewPanel.Children.Clear();
            View.ViewPanel.Children.Add(view);
        }


        /// <summary>
        /// This Method is used to reset the History list and start new list 
        /// </summary>
        /// <param name="UC">
        /// The key of the Usercontrol that will be displayed
        /// </param>
        public virtual void NavigateBranch(BaseUC view)
        {

            var lastView = HistoryLst.LastOrDefault();
            foreach (var removedUC in HistoryLst.Reverse())
            {
                removedUC.RemoveData();
                if (!HistoryLst.Equals(lastView))
                {
                    removedUC.ViewModel = null;
                }
            }

            // Clear the History
            HistoryLst.Clear();
            //Refresh the UC
            view.Refresh();
            //Add it to the history list
            HistoryLst.Add(view);
            //Get target panel
            var View = ViewPanels[ViewPanelKey];
            //Clear the view and display the Usercontrol
            View.ViewPanel.Children.Clear();
            View.ViewPanel.Children.Add(view);

            #region Call GC
            GC.Collect();
            GC.WaitForPendingFinalizers();
            #endregion

        }

        /// <summary>
        /// Used to add new ViewPanel into the navigation service
        /// </summary>
        /// <param name="viewPanelKey">The key of the ViewPanel (should not changed)</param>
        /// <param name="viewPanel">View Panel (contains the View info and methods)</param>
        public static void AddViewPanel(String viewPanelKey, BaseViewPanel viewPanel)
        {
            ViewPanels.Add(viewPanelKey, viewPanel);
        }
        /// <summary>
        /// Used to remove ViewPanel from navigation service
        /// </summary>
        /// <param name="viewPanelKey">The key of the ViewPanel</param>
        public static void RemoveViewPanel(string viewPanelKey)
        {
            ViewPanels.Remove(viewPanelKey);
        }
        #endregion
       
    }
}

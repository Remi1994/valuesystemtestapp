﻿using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using WPF.Core.Views;

namespace WPF.Core.Navigation
{
    public class Dialog
    {
        #region Fields

        private readonly MetroWindow metroWindow;

        private List<DialogUC> dialogsHistory;
        public List<DialogUC> DialogsHistory
        {
            get { return dialogsHistory; }
            set { dialogsHistory = value; }
        }

        private BaseViewPanel dialogView;
        protected BaseViewPanel DialogView
        {
            get { return dialogView; }
            set { dialogView = value; }
        }

        private BaseViewPanel viewPanel;
        public BaseViewPanel ViewPanel
        {
            get { return viewPanel; }
            set { viewPanel = value; }
        }

        #endregion

        #region Constructors
        public Dialog(MetroWindow metroWindow, BaseViewPanel dialogPanel, BaseViewPanel viewPanel)
        {
            this.metroWindow = metroWindow;
            this.DialogView = dialogPanel;
            this.ViewPanel = viewPanel;
            DialogsHistory = new List<DialogUC>();
        }


        #endregion

        #region Methods

        /// <summary>
        /// Used to display a message for the user
        /// </summary>


        public Task ShowMessageAsync(string title, string message)
        {
            return metroWindow.ShowMessageAsync(title, message);
        }
        public Task<ProgressDialogController> ShowProgressAsync(string title, string message)
        {
            return metroWindow.ShowProgressAsync(title, message);
        }
        /// <summary>
        /// Used to display normal Message box

        public MessageDialogResult ShowMessageDialog(string title, string message)
        {
            var messageDialog = new MessageDialog();
            messageDialog.Title = title;
            messageDialog.Message = message;
            messageDialog.ShowDialog();
            return messageDialog.Result;

        }



        #region Dialog Tools


        public void ShowDialog(DialogUC dialog)
        {
            //Clear the Dialog View container
            DialogView.ViewPanel.Children.Clear();
            //Make the DialogView Visible
            DialogView.ViewPanel.Visibility = Visibility.Visible;
            //Disable the ViewPanel
            ViewPanel.ViewPanel.IsEnabled = false;
            //disable the last Dialog (if any)
            if (DialogsHistory.Count > 0)
            {
                DialogsHistory.Last().IsEnabled = false;
            }
            //Add the dialog into Dialog History list  
            DialogsHistory.Add(dialog);
            //Refresh the dialog
            dialog.Refresh();
            //display the dialog
            DialogView.ViewPanel.Children.Add(dialog);
        }

        /// <summary>
        /// Close last Dialog
        /// </summary>
        /// <param name="e"></param>
        public void CloseDialog(CloseEventArgs e)
        {
            var closedDialog = DialogsHistory.Last();
            ////Clear the Dialog Reference
            DialogsHistory.Remove(closedDialog);
            ////Remove last Dialog Data
            closedDialog.RemoveData();
            //Clear current dialog view
            DialogView.ViewPanel.Children.Clear();

            if (DialogsHistory.Count == 0)
            {
                DialogView.ViewPanel.Visibility = Visibility.Collapsed;
                //ReEnable the View
                viewPanel.ViewPanel.IsEnabled = true;

            }
            else if (DialogsHistory.Count > 0)
            {
                //last dialog closing case:
                var lastDialog = DialogsHistory.Last();
                lastDialog.Refresh();
                DialogView.ViewPanel.Children.Add(lastDialog);
            }
            closedDialog.OnClosedEvent(e);
        }


        #endregion



        #endregion
    }
}
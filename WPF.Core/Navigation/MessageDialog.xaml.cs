﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF.Core.Navigation
{
    public enum MessageDialogResult { OK, Cancel }
    /// <summary>
    /// Interaction logic for MessageDialog.xaml
    /// </summary>
    public partial class MessageDialog : MetroWindow
    {
        #region Fields
        #region Message Fields
        public string Message
        {
            get { return (string)GetValue(MessageProperty); }
            set { SetValue(MessageProperty, value); }
        }

        public static readonly DependencyProperty MessageProperty =
            DependencyProperty.Register("Message", typeof(string), typeof(MessageDialog), new PropertyMetadata(null));

        private MessageDialogResult result = MessageDialogResult.Cancel;
        public MessageDialogResult Result
        {
            get { return result; }
            set { result = value; }
        }

        #endregion

        #endregion

        #region Constructors
        public MessageDialog()
        {
            InitializeComponent();
            this.DataContext = this;
        }
        public MessageDialog(string messageTitle, string message) : base()
        {
            Title = messageTitle;
            Message = message;
        }
        #endregion

        #region Methods

        #region Window logic
        private void btn_Cancel_Click(object sender, RoutedEventArgs e)
        {
            Result = MessageDialogResult.Cancel;
            this.Close();
        }
        private void btn_OK_Click(object sender, RoutedEventArgs e)
        {
            Result = MessageDialogResult.OK;
            this.Close();
        }
        #endregion

        #endregion

    }
}

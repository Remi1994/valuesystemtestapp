﻿using System;
using System.Windows.Input;

namespace WPF.Core.Commands
{
    public class RelayCommand : ICommand
    {
        #region Static Fields
        private static bool defaultCanExecute = true;
        /// <summary>
        /// Used to setup the default behaviour for execute commands
        /// default value:true
        /// </summary>
        public static bool DefaultCanExecute
        {
            get { return defaultCanExecute; }
            set { defaultCanExecute = value; }
        }
        #endregion

        #region Fields
        private readonly Action<object> execute;
        private readonly Predicate<object> canExecute;
        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new command that can always execute.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        public RelayCommand(Action<object> execute)
            : this(execute, null)
        {
        }

        /// <summary>
        /// Creates a new command.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        /// <param name="canExecute">The execution status logic.</param>
        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            if (execute == null)
                throw new ArgumentNullException("execute delegate is null ");

            this.execute = execute;
            this.canExecute = canExecute;
        }

        #endregion // Constructors

        #region Methods
        /// <summary>
        /// Used to determine if the Command is executable
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public bool CanExecute(object parameter)
        {
            return canExecute == null ? DefaultCanExecute : canExecute(parameter);
        }

        /// <summary>
        ///  Command Excute logic
        /// </summary>
        /// <param name="parameter"></param>
        public void Execute(object parameter)
        {
            execute(parameter);
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
        #endregion
    }
}

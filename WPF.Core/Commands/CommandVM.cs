﻿using System;
using System.Windows.Input;

namespace WPF.Core.Commands
{
    public class CommandVM
    {
        #region Fields
        /// <summary>
        /// Represet the Command that will be execute
        /// </summary>
        public ICommand Command { get; private set; }

        #endregion

        #region Costructors

        /// <summary>
        ///  Create new Command With Display Name and Command Logic
        /// </summary>
        /// <param name="command">Command Method or logic</param>
        public CommandVM(ICommand command)
        {
            if (command == null)
            {
                throw new ArgumentNullException("No Command Found");
            }
            Command = command;
        }
        #endregion

    }
}

﻿using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using WPF.Core.ViewModels;

namespace WPF.Core.Views
{
    public interface IData
    {
        BaseVM ViewModel { get; }
        /// <summary>
        /// Used to reload the data from the Datassource
        /// </summary>
        void Refresh();
        /// <summary>
        /// Used to deallocate (Remove) Data
        /// </summary>
        void RemoveData();
    }
    public abstract class BaseUC : UserControl, IData
    {

        #region Fields

        #region Abstract Properties
        public abstract BaseVM ViewModel { get; set; }


        #endregion


        #endregion

       
        #region Interface (Abstract Implement)

        #region IData
        /// <summary>
        /// Close the VM and remove the data from it 
        /// </summary>
        public virtual void RemoveData()
        {
            if (ViewModel == null)
            {
                return;
            }
            //Remove the Data from the VM
            ViewModel.removeData();
        }

        /// <summary>
        /// Call the GetData from the VM to grap the data from DataSource
        /// </summary>
        public async virtual void Refresh()
        {
            this.Visibility = Visibility.Collapsed;
            //ViewModel.LoadingMessage = "Loading , Please wait ...";
            ViewModel.startLoading(true);
            DataContext = null;
            await Task.Run(() =>
            {
                ViewModel.getData();
            });
            this.DataContext = ViewModel;
            this.Visibility = Visibility.Visible;
            ViewModel.stopLoading();
        }

        #endregion

        #endregion
    }
}

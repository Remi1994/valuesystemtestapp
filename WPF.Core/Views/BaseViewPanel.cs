﻿using System;
using System.Windows.Controls;

namespace WPF.Core.Views
{
    public class BaseViewPanel
    {
        #region Fields
        private Panel viewPanel;
        public Panel ViewPanel
        {
            get { return viewPanel; }
            set { viewPanel = value; }
        }

        #region Delegates
        private Action startLoading;
        private Action endLoading;

        #endregion
        #endregion
        #region Constructors
        public BaseViewPanel(Panel viewPanel, Action startLoading, Action endLoading)
        {
            ViewPanel = viewPanel;
            this.startLoading = startLoading;
            this.endLoading = endLoading;
        }
        #endregion
        #region Methods
        public void StartLoading()
        {
            startLoading?.Invoke();
        }
        public void StopLoading()
        {
            endLoading?.Invoke();
        }
        #endregion

    }
}

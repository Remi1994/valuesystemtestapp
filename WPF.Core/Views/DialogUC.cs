﻿using System;
using System.Windows;

namespace WPF.Core.Views
{
    /// <summary>
    /// Represents the method that will handle close events for the dialog
    /// </summary>
    /// <param name="sender">The dialog that rase the event</param>
    /// <param name="e">Data associated with the dialog </param>
    public delegate void CloseEventHandler(object sender, CloseEventArgs e);

    public abstract class DialogUC : BaseUC
    {


        #region Fields

        #region Events
        public event EventHandler<CloseEventArgs> ClosedEvent;
        #endregion



        #endregion

        #region Constructors
        static DialogUC()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(DialogUC), new FrameworkPropertyMetadata(typeof(DialogUC)));
        }


        #endregion


        /// <summary>
        /// Used to raise the Close event
        /// </summary>
        /// <param name="e"></param>
        public virtual void OnClosedEvent(CloseEventArgs e)
        {
            ClosedEvent?.Invoke(this, e);
        }



    }

    public class CloseEventArgs : EventArgs
    {
        #region Fields
        private object data;
        /// <summary>
        /// Represent the data sended by the dialog 
        /// </summary>
        public object Data
        {
            get { return data; }
            set { data = value; }
        }

        private DialogResult result;

        /// <summary>
        /// The result of the Dialog
        /// </summary>
        public DialogResult Result
        {
            get { return result; }
            set { result = value; }
        }

        #endregion

    }

    /// <summary>
    /// Used to describe the Dialog status after closing it
    /// </summary>
    public enum DialogResult { Done, Cancel }
}


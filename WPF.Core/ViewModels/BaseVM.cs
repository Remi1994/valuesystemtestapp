﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using WPF.Core.Commands;

namespace WPF.Core.ViewModels
{
    public enum Language { Arabic, English }
    public interface IData
    {
        /// <summary>
        /// This method is being Called when the VM load Data from Database
        /// </summary>
        void getData();

        /// <summary>
        /// This method is being call to remove the datalist from the View-model
        /// Stages:
        /// 1-set all the Datalist(which contains data) to null in order to deallocate them
        /// </summary>
        void removeData();
    }
    public abstract class BaseVM : DependencyObject, IData, INotifyPropertyChanged
    {
        #region Fields

        private static Language currentLanguage;
        public static Language CurrentLanguage
        {
            get { return currentLanguage; }
            set
            {
                currentLanguage = value;
            }
        }
         
         

        private static TextBlock loadingTextBlock;

        public static TextBlock LoadingTextBlock
        {
            get { return loadingTextBlock; }
            set { loadingTextBlock = value; }
        }


       
        public static Border LoadingPanel { get; set; }

        private static List<UIElement> navigationPanels;

        public static List<UIElement> NavigationControls
        {
            get
            {
                if (navigationPanels == null) navigationPanels = new List<UIElement>();
                return navigationPanels;
            }
            set { navigationPanels = value; }
        }
       
        #endregion
         
        #region Methods
         

        #region Dialog Tools

        public void startLoading(bool disableNavigationPanel = false)
        {
            if (LoadingPanel != null) LoadingPanel.Visibility = Visibility.Visible;
            if (disableNavigationPanel) disableNavigation();
        }
        public void stopLoading()
        {
            if (LoadingPanel != null) LoadingPanel.Visibility = Visibility.Collapsed;
            enableNavigation();
        }

        public void enableNavigation()
        {
            foreach (var item in NavigationControls)
            {
                item.IsEnabled = true;
            }
        }
        public void disableNavigation()
        {
            foreach (var item in NavigationControls)
            {
                item.IsEnabled = false;
            }
        }
        
        #endregion

      

        #endregion

        #region interface

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(name));
            }
        }
        #endregion

        #region IData
      
        public abstract void getData();

        public abstract void removeData();
        #endregion

        #endregion

        #region Commands
        private ObservableCollection<CommandVM> cmdLst;
        public ObservableCollection<CommandVM> CmdLst
        {
            get
            {
                if (cmdLst == null) cmdLst = CreateCommands();
                return cmdLst;
            }
        }
        /// <summary>
        /// Create the All Commands and return an ObservableCollection contains them
        /// Exception if the method hasn't overrided yet
        /// </summary>
        /// <returns>ObservableCollection contains the Command</returns>
        public virtual ObservableCollection<CommandVM> CreateCommands()
        {
            throw new NotImplementedException("Override create commands method in your VM");
        }


        #endregion
    }
}

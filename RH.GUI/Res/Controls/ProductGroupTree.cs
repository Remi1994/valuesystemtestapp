﻿using RH.Model.Entities;
using Hardcodet.Wpf.GenericTreeView;
using System.Collections.Generic;
using System.Windows.Controls;

namespace RH.GUI.Res.Controls
{
    public class GroupTree : TreeViewBase<Group>
    {
        public override ICollection<Group> GetChildItems(Group parent)
        {
            return parent.Children;
        }

        public override string GetItemKey(Group item)
        {
            return item.Name;
        }

        public override Group GetParentItem(Group item)
        {
            return item.Parent;
        }
        protected override void ApplySorting(TreeViewItem node, Group item)
        {
            base.ApplySorting(node, item);
        }
    }
}

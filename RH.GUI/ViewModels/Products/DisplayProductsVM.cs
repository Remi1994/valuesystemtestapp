﻿using RH.GUI.WindowsVM;
using RH.Model.Entities;
using RH.Model.Utilities;
using RH.Providers.Product;
using System.Collections.ObjectModel;
using WPF.Core.Commands;
using WPF.Core.Navigation;
using RH.GUI.Views.Products;
using System.Linq;
using System.Collections.Generic;
using RH.Providers.Group;
using RH.GUI.Views.Groups;
using System.Windows;

namespace RH.GUI.ViewModels.Products
{
    public class DisplayProductsVM : MainVM
    {
        #region Fields 

        private BaseProductProvider provider;
        public BaseProductProvider Provider
        {
            get { return provider; }
            set { provider = value; }
        }
        private BaseGroupProvider providerGroup;
        public BaseGroupProvider ProviderGroup
        {
            get { return providerGroup; }
            set { providerGroup = value; }
        }
       
        private ObservableCollection<Product> itemsLst;
        public ObservableCollection<Product> ItemsLst
        {
            get { return itemsLst; }
            set
            {
                itemsLst = value;
                OnPropertyChanged(nameof(FilteredItems));
            }
        }
        public ObservableCollection<Product> FilteredItems
        {
            get
            {
                ObservableCollection<Product> lstFiltered = SelectedGroup == null ? ItemsLst.ToObservableCollection() : new ObservableCollection<Product>();
                var lstGroups = SelectedGroup == null ? new List<Group>() : DFS(SelectedGroup);
                if (SelectedGroup != null) lstGroups.Add(SelectedGroup);
                foreach (var pro in ItemsLst)
                {
                    foreach (var proCat in lstGroups)
                    {
                        if (pro.GroupId == proCat.Id)
                            lstFiltered.Add(pro);

                    }
                }

                if (!string.IsNullOrEmpty(SearchText))
                {
                    lstFiltered = lstFiltered?.Where(c => c.Name.ToUpper().Contains(SearchText.ToUpper())).ToObservableCollection();
                }
                lstResult.Clear();
                return lstFiltered;
            }
        }


      
        private Product selectedItem;
        public Product SelectedItem
        {
            get { return selectedItem; }
            set
            {
                selectedItem = value;
                OnPropertyChanged(nameof(SelectedItem));
            }
        }

        private string searchText;
        public string SearchText
        {
            get { return searchText; }
            set
            {
                searchText = value;
                OnPropertyChanged(nameof(SearchText));
                OnPropertyChanged(nameof(FilteredItems));
            }
        }

        #region Groups

        public List<Group> lstResult = new List<Group>();
        List<Group> DFS(Group proCat)
        {

            if (proCat.Children.Count > 0)
            {
                foreach (Group item in proCat.Children)
                {
                    lstResult.Add(item);

                    DFS(item);
                }
            }

            return lstResult;
        }

        private List<Group> groupsLst = new List<Group>();
        public List<Group> GroupsLst
        {
            get { return groupsLst; }
            set { groupsLst = value; }
        }
        private ObservableCollection<Group> rootsGroupsLst = new ObservableCollection<Group>();
        public ObservableCollection<Group> RootsGroupsLst
        {
            get { return rootsGroupsLst; }
            set
            {
                rootsGroupsLst = value;
                OnPropertyChanged(nameof(RootsGroupsLst));
            }
        }
        private Group selectedGroup;
        public Group SelectedGroup
        {
            get { return selectedGroup; }
            set
            {
                selectedGroup = value;
                OnPropertyChanged(nameof(FilteredItems));
            }
        }
        #endregion
        #endregion

        #region ctor
        public DisplayProductsVM()
        {
            Provider = new DbProductProvider(Context);
            ProviderGroup = new DbGroupProvider(Context);
        

        }
        #endregion

        #region Commands
        public override ObservableCollection<CommandVM> CreateCommands()
        {
            return new ObservableCollection<CommandVM>()
            {
                  // 0
                new CommandVM(new RelayCommand(param => Add())),
                  // 1 
                new CommandVM(new RelayCommand(param => Edit())),
                  // 2
                new CommandVM(new RelayCommand(param => Delete())),
            
                  // 3
                new CommandVM(new RelayCommand(param => AddNewGroup())),
                  // 4
                new CommandVM(new RelayCommand(param => EditGroup())),
                  // 5
                new CommandVM(new RelayCommand(param => DeleteGroup())),
            };
        }

        #endregion

        #region CommandMethods
        #region Groups Methods
        public void AddNewGroup()
        {
            var addEditGroupUC = new AEGroup();
            UserControl.Navigate(addEditGroupUC);
        }
  
        public void EditGroup()
        {
            if (SelectedGroup == null)
            { MessageBox.Show("Please select group to edit!"); return; }
            var addEditGroupUC = new AEGroup(SelectedGroup);
            UserControl.Navigate(addEditGroupUC);
        }
        public void DeleteGroup()
        {
            if (SelectedGroup == null) { MessageBox.Show("Please select group to delete!"); return; }
            if (SelectedGroup.Children.Count > 0 || SelectedGroup.Products.Count > 0) { MessageBox.Show("Please delete sub items first!"); return; }
            else
            {
                var backup = SelectedGroup;
                ProviderGroup.Delete(SelectedGroup.Id);
                GroupsLst.Remove(backup);
                SelectedGroup = null;
                UserControl.NavigateBranch(MainUC["DisplayProducts"]);
            }
        }
        #endregion
        public void Add()
        {
            var addEditUC = new AEProduct("Add");
            UserControl.Navigate(addEditUC);

        }
        public void Edit()
        {
            if (SelectedItem == null) { MessageBox.Show("Please select product to edit!"); return; }
            var addEditUC = new AEProduct(SelectedItem , "Edit" );
            UserControl.Navigate(addEditUC);

        }
         public void DoSth()
        {

        }
        public void Delete()
        {
            var backup = SelectedItem;
            if (SelectedItem == null) { MessageBox.Show("Please select product to delete!"); return; }

            var result = MainVM.Dialog.ShowMessageDialog("Warning","Confirm Delete ?");
            if (result == MessageDialogResult.OK)
            {
                Provider.Delete(SelectedItem.Id);
                ItemsLst.Remove(backup);
                SelectedItem = null;
                OnPropertyChanged(nameof(FilteredItems));
            }
        }

       
        
        #endregion
        public override void getData()
        {
            ItemsLst =  Provider.Query(true). ToObservableCollection();
            var groupsLst = ProviderGroup.Query(true).ToObservableCollection();
            RootsGroupsLst = groupsLst.Where(c => c.Parent == null  ).ToObservableCollection();
            GroupsLst = groupsLst.OrderBy(c => c.Name).BuildComboTree();

        }

        public override void removeData()
        {

        }
    }
}

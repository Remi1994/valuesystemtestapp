﻿
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using WPF.Core.Commands;
using RH.GUI.WindowsVM;
using RH.Model.Entities;
using RH.Providers.Product;
using RH.Model.Utilities;

using RH.Providers.Group;
using System.Windows;

namespace RH.GUI.ViewModels.Products
{
    public class AEProductVM : MainVM
    {
        #region ctor
        public AEProductVM()
        {
            ProviderProduct = new DbProductProvider(Context);
            ProviderGroup = new DbGroupProvider(Context);
           
        }
        #endregion

        #region Fields
        #region Providers
        private BaseGroupProvider providerGroup;
        public BaseGroupProvider ProviderGroup
        {
            get { return providerGroup; }
            set { providerGroup = value; }
        }
        private BaseProductProvider providerProduct;
        public BaseProductProvider ProviderProduct
        {
            get { return providerProduct; }
            set { providerProduct = value; }
        }
      
       
       
        #endregion
        #region SelectedData
     
        private Group selectedGroup;
        public Group SelectedGroup
        {
            get { return selectedGroup; }
            set
            {
                selectedGroup = value;
                OnPropertyChanged(nameof(SelectedGroup));
            }
        }
        #endregion
        #region lists
         private List<Group> groupsLst;
        public List<Group> GroupsLst
        {
            get { return groupsLst; }
            set { groupsLst = value; }
        }
        private ObservableCollection<Group> rootsGroupsLst;
        public ObservableCollection<Group> RootsGroupsLst
        {
            get { return rootsGroupsLst; }
            set
            {
                rootsGroupsLst = value;
                OnPropertyChanged(nameof(RootsGroupsLst));
            }
        }
        #endregion
        private Product product;
        public Product Product
        {
            get { if (product == null) product = new Product() {   }; return product; }
            set
            {
                product = value;
              
            }
        }
       
        #endregion

        #region Commands
        
        public override ObservableCollection<CommandVM> CreateCommands()
        {
            return new ObservableCollection<CommandVM>()
            {

                // 0-Save Product 
                new CommandVM(new RelayCommand(param => Save())),
                // 1-Cancel the Product
                new CommandVM(new RelayCommand(param => CloseWithoutSave())),


            };
        }
        #endregion

        #region Command Methods
       
        public void CloseWithoutSave()
        {
            UserControl.CloseCurrentView();
        }

        public void Save()
        {

            if (string.IsNullOrEmpty(Product.Name)) { MessageBox.Show("Please enter name!"); return; }
               
            if (Product.Id == 0 && providerProduct.Query(false).Any(c => c.Name == Product.Name)) { MessageBox.Show("Name already exist!"); return; }
            if (SelectedGroup == null) { MessageBox.Show("Please Select Group!"); return; }
            Product.GroupId = SelectedGroup.Id;
            ProviderProduct.AddOrUpdate(Product);
           // UserControl.CloseCurrentView();
            UserControl.NavigateBranch(MainUC["DisplayProducts"]);
        }
        #endregion
        public override void getData()
        {
            var groupsLst = ProviderGroup.Query(true).ToList();
            RootsGroupsLst = groupsLst.Where(c => c.Parent == null ).ToObservableCollection();
            GroupsLst = groupsLst.OrderBy(c => c.Name).BuildComboTree();
            if (Product.Id != 0) // edit product
            {
                 SelectedGroup = GroupsLst.First(model => model.Id == Product.GroupId);
                 OnPropertyChanged(nameof(SelectedGroup));
            }
        }

        public override void removeData()
        {
            
        }
     
 
    }
}

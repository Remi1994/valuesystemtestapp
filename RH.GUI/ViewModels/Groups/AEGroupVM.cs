﻿using RH.GUI.WindowsVM;
using RH.Model.Entities;
using RH.Model.Utilities;
using RH.Providers.Group;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using WPF.Core.Commands;

namespace RH.GUI.ViewModels.Groups
{
    public class AEGroupVM :  MainVM
    {
        #region ctor
        public AEGroupVM()
        {
            ProviderGroup = new DbGroupProvider(Context);
        }
        #endregion

        #region fields
        private Group group;
        public Group Group
        {
            get { if (group == null) group = new Group(); return group; }
            set { group = value; }
        }
        private BaseGroupProvider providerGroup;
        public BaseGroupProvider ProviderGroup
        {
            get { return providerGroup; }
            set { providerGroup = value; }
        }

        private List<Group> groupsLst = new List<Group>();
        public List<Group> GroupsLst
        {
            get { return groupsLst; }
            set { groupsLst = value; }
        }
        private ObservableCollection<Group> rootsGroupsLst = new ObservableCollection<Group>();
        public ObservableCollection<Group> RootsGroupsLst
        {
            get { return rootsGroupsLst; }
            set
            {
                rootsGroupsLst = value;
                OnPropertyChanged(nameof(RootsGroupsLst));
            }
        }

        #endregion

        #region Commands 
        public override ObservableCollection<CommandVM> CreateCommands()
        {
            return new ObservableCollection<CommandVM>()
            {
                // 1-Save
                new CommandVM(new RelayCommand(param=>Save())),
                // 2-Close without save
                new CommandVM(new RelayCommand(param=>CloseWithoutSave()))

            };
        }
        #endregion

        #region methods
        public void Save()
        {
            if (string.IsNullOrEmpty(Group.Name))
            {
                MessageBox.Show("Please enter name!"); 
                return;
            }
            
            if (Group.Id == 0 && ProviderGroup.Query(false).Any(c => c.Name == Group.Name)) { MessageBox.Show("Name already exist!"); return; }
            if (Group.Id == 0)
            {
                if (Group.Parent != null)
                {
                    Group.TreeName = new string(' ', 4) + Group.Parent.TreeName.Replace(Group.Parent.Name, Group.Name);
                    GroupsLst.Insert(GroupsLst.IndexOf(Group.Parent) + 1, Group);
                }
                else
                {
                    //add the new Item to the tree view list and the combobox list
                    Group.TreeName = "• " + Group.Name;
                    GroupsLst.Insert(0, Group);
                    RootsGroupsLst.Add(Group);
                }
              
            }
            ProviderGroup.AddOrUpdate(Group);
           
            UserControl.NavigateBranch(MainUC["DisplayProducts"]);
            Group = null;
        }


        public void CloseWithoutSave()
        {
            UserControl.NavigateBranch(MainUC["DisplayProducts"]);
            Group = null;
        }
        public override void getData()
        {
            var groupsLst = Context.Groups.ToList();
            RootsGroupsLst = groupsLst.Where(c => c.Parent == null  ).ToObservableCollection();
            GroupsLst = groupsLst.OrderBy(c => c.Name).BuildComboTree();
            if (Group.Id > 0)
                Group.Parent = GroupsLst.SingleOrDefault(c => c.Id == Group.ParentId);
        }

        public override void removeData()
        {
            Context = null;
        }
        #endregion
    }
}

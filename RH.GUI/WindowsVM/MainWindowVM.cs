﻿using System.Collections.ObjectModel;
using WPF.Core.Commands;

namespace RH.GUI.WindowsVM
{
    public class MainWindowVM : MainVM
    {
        // create commands
        public override ObservableCollection<CommandVM> CreateCommands()
        {
            return new ObservableCollection<CommandVM>()
            {
                new CommandVM(new RelayCommand(param=> viewMainUC(param.ToString())))
            };
        }
        //  to display the required view from main window
        public void viewMainUC(string mainUCKey)
        {
            UserControl.NavigateBranch(MainUC[mainUCKey]);
        }

        #region data 
        public override void getData()
        {

        }

        public override void removeData()
        {

        }
        #endregion 
    }
}

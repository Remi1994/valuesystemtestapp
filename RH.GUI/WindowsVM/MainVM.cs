﻿using RH.DbInitializer;
using RH.Model.Entities;
using System.Collections.Generic;
using WPF.Core.Navigation;
using WPF.Core.ViewModels;
using WPF.Core.Views;

namespace RH.GUI.WindowsVM
{
    public class MainVM : BaseVM
    {
        // view name key for view and dialog
        public const string NavigationPanelKey = "MainView";

        private static View userControl;
        public static View UserControl
        {
            get { return userControl; }
            set { userControl = value; }
        }


        private static Dialog dialog;
        public static Dialog Dialog
        {
            get { return dialog; }
            set { dialog = value; }
        }
        // main views (User Controls)
        private static Dictionary<string, BaseUC> mainUC = new Dictionary<string, BaseUC>();
        public static Dictionary<string, BaseUC> MainUC
        {
            get { return mainUC; }
            set { mainUC = value; }
        }
         
        // project context
        public static Application_Context ContextBuilder { get { return new Application_Context(); } }

        private Application_Context context;
        public Application_Context Context
        {
            get { if (context == null) context = ContextBuilder; return context; }
            set {  context = value;  }
        }

        

        // implementing abstract class
        public override void getData()
        {
            
        }

        public override void removeData()
        {
            
        }
    }
}

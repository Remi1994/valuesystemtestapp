﻿using RH.GUI.ViewModels.Groups;
using RH.Model.Entities;
using WPF.Core.ViewModels;
using WPF.Core.Views;

namespace RH.GUI.Views.Groups
{
    /// <summary>
    /// Interaction logic for AEGroup.xaml
    /// </summary>
    public partial class AEGroup : BaseUC
    {
        public AEGroup()
        {
            InitializeComponent();
            tb_Title.Text ="Add";
            
        }
        public AEGroup(Group group)
        {
            InitializeComponent();
            VM.Group = group;
            tb_Title.Text = "Edit";
 
        }
        public override BaseVM ViewModel
        {
            get
            {
                if (vm == null) vm = new AEGroupVM(); return vm;
            }

            set
            {
                vm = value as AEGroupVM;
            }
        }

        private AEGroupVM vm;

        public AEGroupVM VM
        {
            get { if (vm == null) vm = new AEGroupVM(); return vm; }
            set { vm = value; }
        }
    }
}

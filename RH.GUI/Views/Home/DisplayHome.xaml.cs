﻿using RH.GUI.ViewModels.Home;
using WPF.Core.ViewModels;
using WPF.Core.Views;

namespace RH.GUI.Views.Home
{
    /// <summary>
    /// Interaction logic for DisplayHome.xaml
    /// </summary>
    public partial class DisplayHome : BaseUC
    {
        public DisplayHome()
        {
            InitializeComponent();
        }

        public override BaseVM ViewModel
        {
            get
            {
                if (vm == null) vm = new HomeVM(); return vm;
            }

            set
            {
                vm = value as HomeVM;
            }
        }

        private HomeVM vm;

        public HomeVM VM
        {
            get { if (vm == null) vm = new HomeVM(); return vm; }
            set { vm = value; }
        }

    }
}

﻿using RH.GUI.ViewModels.Products;
using WPF.Core.ViewModels;
using WPF.Core.Views;

namespace RH.GUI.Views.Products
{
    /// <summary>
    /// Interaction logic for DisplayProducts.xaml
    /// </summary>
    public partial class DisplayProducts : BaseUC
    {
       
        public DisplayProducts()
        {
            InitializeComponent();
          
                tb_Title.Text ="Products";
            
        }
        public override BaseVM ViewModel
        {
            get
            {
                if (vm == null) vm = new DisplayProductsVM(); return vm;
            }

            set
            {
                vm = value as DisplayProductsVM;
            }
        }

        private DisplayProductsVM vm;

        public DisplayProductsVM VM
        {
            get { if (vm == null) vm = new DisplayProductsVM(); return vm; }
            set { vm = value; }
        }
    }

}

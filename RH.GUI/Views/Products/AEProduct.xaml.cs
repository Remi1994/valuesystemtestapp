﻿using RH.GUI.ViewModels.Products;
using RH.Model.Entities;
using System;
using System.Windows;
using WPF.Core.ViewModels;
using WPF.Core.Views;

namespace RH.GUI.Views.Products
{
    /// <summary>
    /// Interaction logic for AEProduct.xaml
    /// </summary>
    public partial class AEProduct : BaseUC
    {
        public AEProduct(string title)
        {
            InitializeComponent();
            tb_Title.Text = title;
        
        }
        public AEProduct(Product product, string title )
        {
            InitializeComponent();
            tb_Title.Text = title;
            VM.Product = product;
         
          
        }
      
        public override BaseVM ViewModel
        {
            get
            {
                if (vm == null) vm = new AEProductVM( ); return vm;
            }

            set
            {
                vm = value as AEProductVM;
            }
        }

        private AEProductVM vm;

        public AEProductVM VM
        {
            get { if (vm == null) vm = new AEProductVM( ); return vm; }
            set { vm = value; }
        }


     
       

      
    }
}

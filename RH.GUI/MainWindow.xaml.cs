﻿
using RH.GUI.Views.Home;

using RH.GUI.Views.Products;

using RH.GUI.WindowsVM;
using MahApps.Metro.Controls;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using WPF.Core.Navigation;
using WPF.Core.Views;

namespace RH.GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private MainWindowVM vm;
        public MainWindowVM VM
        {
            get { if (vm == null) vm = new MainWindowVM(); return vm; }
            set { vm = value; }
        }
       
       

        private void setPanels()
        {
            MainWindowVM.LoadingPanel = loadingPanel;
            MainWindowVM.LoadingTextBlock = loadingTextBlock;
            MainWindowVM.NavigationControls.Add(stack_NavBar);
            MainWindowVM.NavigationControls.Add(grd_RootView);
        }
        private void setUpMainView()
        {
            MainVM.MainUC.Add("DisplayHome", new DisplayHome());
            MainVM.MainUC.Add("DisplayProducts", new DisplayProducts());
        }
        private void setNavigationServices()
        {
            var mainVMViewPanel = new BaseViewPanel(grd_MainView, null, null);
            View.AddViewPanel(MainVM.NavigationPanelKey, mainVMViewPanel);
            //set mainView navigation Panel
            MainVM.UserControl = new View(MainVM.NavigationPanelKey);
             MainVM.Dialog = new Dialog(this, new  BaseViewPanel(grd_MainDialogView, null, null), mainVMViewPanel);

        }
        
        public MainWindow()
        {
            setUpMainView();
            InitializeComponent();
            setNavigationServices();
            setPanels();
            
        }
        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            Task.Run(() => { VM.getData(); });
            this.DataContext = VM;
        }

       

        
        private void wind_Main_Closed(object sender, EventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
